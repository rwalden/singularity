//
//  UnityContainerExtension.h
//  Singularity
//
//  Created by Rhys Walden on 7/12/13.
//
//

#import <Foundation/Foundation.h>
#import "SingularityContainerExtensionConfigurator.h"

@interface SingularityContainerExtension : NSObject <SingularityContainerExtensionConfigurator>

@end
