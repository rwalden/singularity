//
//  UnityContainerExtension.m
//  Singularity
//
//  Created by Rhys Walden on 7/12/13.
//
//

#import "SingularityContainer.h"
#import "SingularityContainerExtension.h"
#import "SingularityContainerExtensionConfigurator.h"

@implementation SingularityContainerExtension

- (instancetype)initWithSingularityContainer:(id<SingularityContainer>)singularityContainer
{
    if (self = [super init])
    {
        _singularityContainer = singularityContainer;
    }
    
    return self;
}


@synthesize singularityContainer = _singularityContainer;

@end

