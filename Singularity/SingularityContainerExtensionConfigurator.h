//
//  SingularityContainerExtensionConfigurator.h
//  Singularity
//
//  Created by Rhys Walden on 7/12/13.
//
//

#import <Foundation/Foundation.h>
#import "SingularityContainer.h"

@protocol SingularityContainerExtensionConfigurator <NSObject>

@property (nonatomic, strong, readonly) id<SingularityContainer>singularityContainer;

@end
